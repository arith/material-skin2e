﻿namespace MaterialSkin.Controls
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Windows.Forms;

    public sealed class MaterialDivider2 : Control, IMaterialControl
    {
        [Browsable(false)]
        public int Depth { get; set; }

        [Browsable(false)]
        public MaterialSkinManager SkinManager => MaterialSkinManager.Instance;

        [Browsable(false)]
        public MouseState MouseState { get; set; }
        //divider min height
        private const int MIN_DIVIDER_HEIGHT = 5;
        //left divider and right divider's total min width
        private const int MIN_LEFT_RIGHT_Width = 10;
        private Size titleSize = default;
        private Rectangle titleRect = new Rectangle();

        private Padding margin;


        private TitlePosition _titlePosition = TitlePosition.Center;
        [Category("Material")]
        public TitlePosition TitlePosition
        {
            get
            {
                return _titlePosition;
            }
            set
            {
                //TitlePosition oldTitlePosition = _titlePosition;
                _titlePosition = value;
                allocatePositonSize();
                Invalidate();
            }
        }


        private string _title = string.Empty;
        [Category("Material"), Localizable(true)]
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value.Trim();

                //SizeF oldTitleSize = this.titleSize;
                if (!string.IsNullOrWhiteSpace(_title))
                {
                    computeTitleize(_title);
                }
                allocatePositonSize();
                Invalidate();
            }
        }

        private Rectangle leftDivider = default;
        private Rectangle rightDivider = default;
        public MaterialDivider2()
        {
            margin = new Padding(8, 4, 8, 4);
            //SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            Height = MIN_DIVIDER_HEIGHT;
            //BackColor = SkinManager.BackgroundColor;//SkinManager.DividersColor;
            Font = SkinManager.getFontByType(MaterialSkinManager.fontType.Subtitle2);
            ForeColor = SkinManager.TextHighEmphasisColor;
            //if true, the text is clear
            DoubleBuffered = true;
            //
            leftDivider = new Rectangle(0, this.Height / 2, this.Width, 1);
            rightDivider = leftDivider;
        }

        private void computeTitleize(string text)
        {

            using (Graphics g = this.CreateGraphics())
            {
                if (!string.IsNullOrWhiteSpace(text))
                {
                    //StringFormat sf = StringFormat.GenericTypographic;
                    //sf.FormatFlags |= StringFormatFlags.MeasureTrailingSpaces;
                    //titleSize = g.MeasureString(text.ToUpper(), SkinManager.getFontByType(MaterialSkinManager.fontType.Subtitle2), PointF.Empty, sf).ToSize();
                    using (NativeTextRenderer NativeText = new NativeTextRenderer(CreateGraphics()))
                    {
                        titleSize = NativeText.MeasureLogString(_title, SkinManager.getLogFontByType(MaterialSkinManager.fontType.Subtitle2));
                        titleSize.Width += 1; // necessary to avoid a bug when autosize = true
                    }

                }
                else
                {
                    titleSize.Width = 0;
                    titleSize.Height = 0;
                }
            }
        }

        private void allocatePositonSize() //(TitlePosition oldTitlePosition, SizeF oldTitleSize)
        {
            if (string.IsNullOrWhiteSpace(_title))
            {
                leftDivider.Width = this.Width;
                rightDivider.Width = 0;
                leftDivider.Location = new Point(0, this.Height / 2);
                rightDivider.Location = leftDivider.Location;
                titleRect.Width = titleRect.Height = 0;
            }
            else
            {
                //if(titlePosition != oldTitlePosition)
                //{
                //    leftDivider.Width = this.Width;
                //    rightDivider.Width = 0;
                //}
                //                SizeF oldTitleSize = this.titleSize;
                //                computeTitleize(title);

                if (this.Height < titleSize.Height)
                    this.Height = titleSize.Height;//Convert.ToInt32(Math.Ceiling(titleSize.Height));

                int space = margin.Horizontal + titleSize.Width;

                if (this.Width < MIN_LEFT_RIGHT_Width + space)
                    this.Width = MIN_LEFT_RIGHT_Width + space;

                leftDivider.Width = this.Width;

                //if(rightDivider.Width == 0)
                //{
                if (titleSize.Width >= leftDivider.Width)
                {
                    //leftDivider.Width = 0;
                    return;
                }
                //total width
                int width = leftDivider.Width;
                //left and right margin ,and title width
                //float space = margin.Horizontal + titleSize.Width;
                if (_titlePosition == TitlePosition.Center)
                {
                    leftDivider.Width = width / 2;
                }
                else if (_titlePosition == TitlePosition.Left)
                {
                    leftDivider.Width = width / 4;
                }
                else if (_titlePosition == TitlePosition.Right)
                {
                    leftDivider.Width = width * 3 / 4;
                }
                rightDivider.Width = width - leftDivider.Width;
                leftDivider.Width -= space / 2;// Convert.ToInt32(space / 2);
                rightDivider.Width -= space / 2;// Convert.ToInt32(space / 2);
                if(_titlePosition == TitlePosition.Left && leftDivider.Width <= 0)
                {
                    leftDivider.Width = MIN_LEFT_RIGHT_Width / 2;
                    rightDivider.Width = width - leftDivider.Width - space;
                }
                else if(_titlePosition == TitlePosition.Right && rightDivider.Width <= 0)
                {
                    rightDivider.Width = MIN_LEFT_RIGHT_Width / 2;
                    leftDivider.Width = width - rightDivider.Width - space;
                }

                leftDivider.Location = new Point(0, this.Height / 2);
                rightDivider.Location = new Point(width - rightDivider.Width, this.Height / 2);

                //}
                ////rightDivider.Width > 0
                //else
                //{
                //    if (titleSize.Width >= rightDivider.Location.X + rightDivider.Width)
                //    {
                //        return;
                //    }
                //    float bias = (oldTitleSize.Width - this.titleSize.Width) / 2;
                //    float oldSpace = margin.Horizontal + oldTitleSize.Width;
                //    leftDivider.Width += Convert.ToInt32(bias);
                //    rightDivider.Width += Convert.ToInt32(bias);

                //    rightDivider.Location = new Point(rightDivider.Location.X - Convert.ToInt32(bias), rightDivider.Location.Y);

                //}
                //
                titleRect.Width = space;//Convert.ToInt32(titleSize.Width + margin.Horizontal);
                titleRect.Height = this.Height;//Convert.ToInt32(this.Height);
                //add space to left when the position is left
                titleRect.X = leftDivider.Width;
                titleRect.Y = 0;

            }

        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            allocatePositonSize();

            Invalidate();
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);

            if (Height < MIN_DIVIDER_HEIGHT)
                Height = MIN_DIVIDER_HEIGHT;

            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;

            e.Graphics.Clear(Parent.BackColor);

            using (var dividerPen = new Pen(SkinManager.DividersColor, 1))
            {
                // left divider of title
                g.DrawLine(
                        dividerPen,
                        leftDivider.X,
                        leftDivider.Y,
                        leftDivider.X + leftDivider.Width,
                        leftDivider.Y
                   );
                // right divider of title
                g.DrawLine(
                        dividerPen,
                        rightDivider.X,
                        rightDivider.Y,
                        rightDivider.X + rightDivider.Width,
                        rightDivider.Y);
            }

            //Draw title
            using (NativeTextRenderer NativeText = new NativeTextRenderer(g))
            {
                // Draw header text
                NativeText.DrawTransparentText(
                    _title,
                    SkinManager.getLogFontByType(MaterialSkinManager.fontType.Subtitle2),
                    SkinManager.TextHighEmphasisColor,
                    titleRect.Location,
                    titleRect.Size,
                    NativeTextRenderer.TextAlignFlags.Center | NativeTextRenderer.TextAlignFlags.Middle);
            }

        }

    }

    public enum TitlePosition
    {
        Left,
        Center,
        Right
    }
}